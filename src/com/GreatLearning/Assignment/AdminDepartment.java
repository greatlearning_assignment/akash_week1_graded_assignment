package com.GreatLearning.Assignment;

public class AdminDepartment extends SuperDepartment{
	public String departmentName() {
		return "Admin Department"; 
	}
	public String getTodaysWork() {
		return "Complete your documents";
	}
	public String getWorkDeadline() {
		return "Complete by EOD";

}
}
